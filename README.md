# README #


### What is this repository for? ###
Article-api is a backend for creating and editing articles.

It supports following:
 * User signup and login
 * Article create, edit and view
 * Everyone can see all articles
 * Create and edit is restricted only for authors that have account


### How do I get set up? ###

* Execute sql script from sqlDB project to prepare database
* Build and run project
* Please note the url that your application have you might need it for frontend


### TODO

* Email confirmation
* Password reset
* Article pagination
* Refactor
