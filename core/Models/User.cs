﻿using System;
using System.Collections.Generic;

#nullable disable

namespace core.Models
{
    public partial class User
    {
        public User()
        {
            Authors = new HashSet<Author>();
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsConfirmed { get; set; }
        public string Salt { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Author> Authors { get; set; }
    }
}
