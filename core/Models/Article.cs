﻿using System;
using System.Collections.Generic;

#nullable disable

namespace core.Models
{
    public partial class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public int AuthorId { get; set; }

        public virtual Author Author { get; set; }
    }
}
