﻿using data.Models;
using data.Models.User;

namespace data.Interface
{
    public interface IUserRepository
    {
        void Create(UserCreateDTO userCreateDTO, string salt);
        UserCheck GetByEmail(string email);
    }
}
