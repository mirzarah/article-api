﻿using data.Models;
using System.Collections.Generic;

namespace data.Interface
{
    public interface IArticleRepository
    {
        void Create(ArticleCreateDTO articleCreateDTO, int userId);
        ArticleGetDTO GetById(int id);
        List<ArticleGetDTO> GetAll(int? userId = null);
        void Update(ArticleEditDTO articleEditDTO, int userId);
    }
}
