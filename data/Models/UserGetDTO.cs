﻿namespace data.Models
{
    public class UserGetDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
    }
}
