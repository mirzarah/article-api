﻿namespace data.Models
{
    public class ArticleCreateDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
