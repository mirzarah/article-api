﻿using core.Models;
using data.Interface;
using data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace data.Implementation
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly NewsDBContext _context;
        public ArticleRepository(NewsDBContext context)
        {
            _context = context;
        }

        public void Create(ArticleCreateDTO articleCreateDTO, int userId)
        {
            int? authorId = _context.Authors.Where(x => x.UserId == userId)
                .Select(x => x.Id)
                .FirstOrDefault();

            if (!authorId.HasValue) throw new Exception("No such author");

            var article = new Article
            {
                Title = articleCreateDTO.Title,
                Description = articleCreateDTO.Description,
                Created = DateTime.Now,
                AuthorId = authorId.Value
            };

            _context.Articles.Add(article);
            _context.SaveChanges();
        }

        public void Update(ArticleEditDTO articleEditDTO, int userId)
        {
            var authorIds = _context.Authors.Where(x => x.UserId == userId)
                .Select(x => x.Id)
                .ToList();

            if (authorIds.Count == 0) throw new Exception("No such author");

            var articleDB = _context.Articles.Where(x => x.Id == articleEditDTO.Id)
                .FirstOrDefault();

            if (articleDB == null) throw new Exception("No such article");

            var belongsToUser = authorIds.Contains(articleDB.AuthorId);

            if (!belongsToUser) throw new Exception("Cant edit others articles");
           

            articleDB.Title = articleEditDTO.Title;
            articleDB.Description = articleEditDTO.Description;
            _context.Articles.Update(articleDB);
            _context.SaveChanges();
        }

        public ArticleGetDTO GetById(int id)
        {
            var articleDTO = _context.Articles.Where(x => x.Id == id)
                .Select(x=> new ArticleGetDTO
                {
                    Id = x.Id,
                    Description = x.Description,
                    Title = x.Title,
                    Created = x.Created,
                })
                .FirstOrDefault();

            if (articleDTO == null) throw new Exception("No such article!");

            return articleDTO;
        }

        public List<ArticleGetDTO> GetAll(int? userId)
        {
            IQueryable<Article> articles = _context.Articles;

            if (userId.HasValue)
            {
                articles = articles.Where(x => x.Author.UserId == userId);
            }

            return articles
                .OrderByDescending(x => x.Created)
                .Select(x => new ArticleGetDTO
                {
                    Id = x.Id,
                    Description = x.Description,
                    Title = x.Title,
                    Created = x.Created,
                }).ToList();
        }
    }
}
