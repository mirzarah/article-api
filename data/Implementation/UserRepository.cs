﻿using core.Models;
using data.Interface;
using data.Models;
using data.Models.User;
using System.Linq;

namespace data.Implementation
{
    public class UserRepository : IUserRepository
    {
        private readonly NewsDBContext _context;
        public UserRepository(NewsDBContext context)
        {
            _context = context;
        }

        public void Create(UserCreateDTO userCreateDTO, string salt)
        {
            var emailExists = _context.Users.Where(x => x.Email == userCreateDTO.Email)
                .FirstOrDefault();

            if (emailExists != null) throw new System.Exception("User with that email already exists!");

            var user = new User
            {
                UserName = userCreateDTO.UserName,
                Password = userCreateDTO.Password,
                Email = userCreateDTO.Email,
                IsConfirmed = false,
                Salt = salt
            };

            var author = new Author
            {
                FirstName = userCreateDTO.FirstName,
                LastName = userCreateDTO.LastName,
                User = user
            };

            _context.Users.Add(user);
            _context.Authors.Add(author);
            _context.SaveChanges();
        }

        public UserCheck GetByEmail(string email)
        {
            var user =_context.Users.Where(x => x.Email == email)
                .Select(x => new UserCheck
                {
                    Id = x.Id,
                    Email = x.Email,
                    Password = x.Password,
                    Salt = x.Salt
                })
                .FirstOrDefault();

            return user;
        }
    }
}
