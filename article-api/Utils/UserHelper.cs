﻿using System.Security.Claims;

namespace article_api.Utils
{
    public static class UserHelper
    {
        public static int Id(ClaimsPrincipal User)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            bool valid = int.TryParse(userId, out int id);
            if (!valid)
                id = 0;

            return id;
        }
    }
}
