﻿using article_api.Utils;
using data.Interface;
using data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace article_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly IArticleRepository _articleRepository;
        public ArticleController(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        [HttpGet("{id}")]
        public ArticleGetDTO ArticleGet(int id)
        {
            return _articleRepository.GetById(id);
        }

        [HttpGet("getAll")]
        public List<ArticleGetDTO> ArticleAll()
        {
            return _articleRepository.GetAll();
        }

        [HttpGet("getAllMine")]
        [Authorize]
        public List<ArticleGetDTO> ArticleAllMine()
        {
            int id = UserHelper.Id(User);
            return _articleRepository.GetAll(id);
        }
        [HttpPut]
        [Authorize]
        public IActionResult ArticleUpdate(ArticleEditDTO articleEditDTO)
        {
            int id = UserHelper.Id(User);
            _articleRepository.Update(articleEditDTO, id);
            return Ok();
        }

        [HttpPost]
        [Authorize]
        public IActionResult ArticlePost(ArticleCreateDTO articleCreateDTO)
        {
            int id = UserHelper.Id(User);
            _articleRepository.Create(articleCreateDTO, id);
            return Ok();
        }
    }
}
