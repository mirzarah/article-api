﻿using article_api.Utils;
using data.Interface;
using data.Models;
using Microsoft.AspNetCore.Mvc;

namespace article_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost("signup")]
        public IActionResult Signup(UserCreateDTO userCreateDTO)
        {
            string salt = PasswordHash.GetSalt();
            userCreateDTO.Password = PasswordHash.Hash(userCreateDTO.Password, salt);
            _userRepository.Create(userCreateDTO, salt);
            return Ok();
        }

        [HttpPost("login")]
        public IActionResult Login(UserLoginDTO userLoginDTO)
        {
            var user = _userRepository.GetByEmail(userLoginDTO.Email);

            if (user == null) throw new System.Exception("No such user");
            var hashed = PasswordHash.Hash(userLoginDTO.Password, user.Salt);

            if(hashed != user.Password)
            {
                throw new System.Exception("Passwords do not match!");
            }

            var token = TokenManager.GenerateToken(userLoginDTO, user.Id);

            return Ok(new UserGetDTO { 
                Id = user.Id,
                Email = user.Email,
                Token = token
            });
        }
    }
}
