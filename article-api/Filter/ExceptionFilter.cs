﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace article_api.Filter
{
    public class ExceptionFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context) { }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception is Exception exception)
            {
                context.Result = new BadRequestObjectResult(exception.Message)
                {
                    Value = exception.Message,
                };

                context.ExceptionHandled = true;
            }
        }
    }
}
