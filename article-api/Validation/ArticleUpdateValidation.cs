﻿using data.Models;
using FluentValidation;

namespace article_api.Validation
{
    public class ArticleUpdateValidation : AbstractValidator<ArticleEditDTO>
	{
		public ArticleUpdateValidation()
		{
			RuleFor(x => x.Title).NotEmpty().MaximumLength(255);
			RuleFor(x => x.Description).NotEmpty().MaximumLength(2000);
		}
	}
}
