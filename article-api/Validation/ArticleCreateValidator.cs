﻿using data.Models;
using FluentValidation;

namespace article_api.Validation
{
    public class ArticleCreateValidator : AbstractValidator<ArticleCreateDTO>
    {
        public ArticleCreateValidator()
        {
            RuleFor(x => x.Title).NotEmpty().MaximumLength(255);
            RuleFor(x => x.Description).NotEmpty().MaximumLength(2000);
        }
    }
}
